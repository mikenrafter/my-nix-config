{
  inputs.nixpkgs.url = github:NixOS/nixpkgs/0026c79c5509d9d170e85cab71ed7c256b4b0c7b;
  outputs = { self, nixpkgs, flake-utils, git-ignore-nix }: # list out the dependencies
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {inherit system;};
      name = "yi";
      lang = "haskell";
      set  = l: "${l}Packages";
      bldr = "callCabal2Nix";
      buildA = self: super: n: s: b: p: {
        ${n} = self.${b} name (git-ignore-nix.lib.gitignoreSource p) { };
      };
      buildB = self: super: n: s: b: p: {
        ${n} = with npkgs; stdenv.mkDerivation {
          name = n;
          src = p;
          nativeBuildInputs = deps npkgs;
          buildPhase = b;
        }; };
      haskell = self: super: n: p: env: {
        super.haskellPackages.developPackage {
          returnShellEnv = env;
          name = n;
          root = p;
          modifier = drv: (
            super.haskell.lib.compose.addBuildTools (with super.haskellPackages; super.lib.optional returnShellEnv [
              cabal-install
          ]) drv
        );
      }};

      project = self: super: haskell self super name ./yi;

# sub-dependencies
deps = p: (with p.${set lang}; [
]) // (with p; [
]);

      overlay = self: super: {
         "${set lang}" = super.${set lang}.override (old: {
           overrides = super.lib.composeExtensions (old.overrides or (_: _: {}))
             (self: super:

# overrides {
(  project self super false
)
# }

});
};
);

    in {inherit overlay;} // flake-utils.lib.eachDefaultSystem (system: # leverage flake-utils
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ overlay ];
        };
      in {
        defaultPackage = project pkgs pkgs false;
        devShell = project pkgs pkgs true
      });
}

