##bash ~/.nix/build.sh

# my NIX config

inputs: with inputs; with builtins; with lib; let

# util {
basic-conf  = n: attrsets.genAttrs [ n ] (x: {source=./config     + ("/" + x); target="./" + x;});
dot-config  = n: attrsets.genAttrs [ n ] (x: {source=./config/dot + ("/" + x); target="./.config/" + x;});
pattern-map = p: f: t:  libs.runCommand "map" {} "mkdir -p $out/${t}; ln -s ${f}/${p} $out/${t}/";
blind-map   = from: to: pattern-map "*" from to; #link all files
# }

# aliases {
# flake.nix: u1,u2,etc
ifNix = package: mkIf nixos package;
#}
#{ file tracking
files = [
  (pattern-map "*.policy" ./polkit  "share/polkit-1/actions")
  (blind-map              ./scripts "bin")
  (blind-map              ./scripts "scripts") # alternate
  (blind-map              ./apps    "share/applications")
];
#}
# custom packages {
custom = map (x: lib.hiPrio (import x inputs)) [
  ./programs/nvim.nix
  ./programs/quickemu.nix
# ./programs/yi.nix
# ./programs/dolphin-emu.nix
];
#}
# package groups {
# libre-kernel {
kernel = with def.linuxPackages-libre; [

# wireguard                 # VPN method - included in latter kernels


# -}
# unfree {
]; unfree = with inputs.unfree.unstable1; [

##games:
  lutris                    # integrates with steam - proprietary...
  steam                     # games, proprietary, yep
  polymc                    # better minecrft launcher, except minecraft is unfree...


#}
# core {
]; core = with def; [

  nixos-install-tools       # for complete dominion of all the drives!
  coreutils                 # don't piss GNU+Stallman off!
  libtool                   # more gnu tools
  lsof strace               # identify open files
  usbutils                  # lsusb+
  fish dash bash            # main shell & sh + bash, for compatability
  u1.vim                    # editor le supreme
  xterm                     # basic terminal, as a backup only
  bitwarden                 # Password manager, it's that simple :)
  gnome.gnome-keyring       # Keyring, manages DBs other than pw-managers

  auto-cpufreq              # dynamic CPU frequency scaler
  earlyoom                  # oomKiller sucks! But this doesn't.

  nitrogen                  # wallpaper manager
#?picom                     # compositor

#Bcmake                     # for source builds
#Bbinutils                  # for source builds
#Bgcc                       # compiler
  xsecurelock               # secure lockscreen
  xss-lock                  # lockscreen as screensaver
  nix-bundle                # https://github.com/matthewbauer/nix-bundle
  manix nixdoc     # nix documentation
  nix-diff                  # explain why two nix derivations differ
#HDD freefall                 # anti HDD death
  libsecret                 # secret storage
  gnome.gnome-keyring       # secret storage
  gnome.libgnome-keyring    # secret storage
  gnome.seahorse            # secret management


#}
# etc {
]; etc = with def; [

#+ghacks-user.js
#+yacy.drv
#+auto-cpufreq.drv
#+onlyoffice.drv
#+feral/gamemode.drv
#Bgolly                     # Game of Life
  nur.repos.onny.librewolf-bin  # TODO
 #(ifNix librewolf)         # TODO
  s1.firefox                # FF meh, librewolf tho
  brave chromium            # backup browsers
  browsh                    # TUI browser
  thunderbird               # Mozillas mail client
  mailspring                # email client
  libreoffice               # office suite
#Bgodot                     # game engine
  legendary-gl #Bheroic     # "epic" game yoinking
  pidgin                    # chat client
  tut                       # TUI mastodon client
  u2.element-desktop        # matrix client
  cinny                     # matrix client
  emote                     # emoji picker


#}
# code {
]; code = with def; [

# lang cores
# ghc                       # haskell compiler | INCLUDED IN packages.haskell
  rustc                     # rust compiler
  nim                       # nim compiler
  jdk                       # java runtime
  (ifNix python27)          # python interpreter 2.7
  (ifNix python38)          # python interpreter 3.8

# package managers
  cargo                     # rust package manager
  poetry                    # python package-manager
  cabal-install             # haskell package-manager
  stack                     # haskell package-manager
  yarn                      # JS dep-manager | coc.vim needs it

  cabal2nix                 # haskell packager

#}
# editors {
]; editors = with s2; [

  arduino                   # arduino programming
  nvimpager                 # why not?
# yi yi-rope                # haskellEditor
##nvim                      # my custom nvim
  amp kakoune               # similar to vim
  micro                     # similar to nano
  notepadqq                 # GUI text editor
  nano gnome.gedit          # basic AF editor
  sc-im                     # command-line spreadsheet program
  u1.languagetool           # proofread tool


#}
# fonts {
]; fonts = with def; [

  (nerdfonts.override {
	fonts = [
		"Mononoki"
		"Meslo"
		"FiraCode"
		"Terminus"
  ];
  })

  noto-fonts
  noto-fonts-cjk
  noto-fonts-emoji
  liberation_ttf
  s1.mplus-outline-fonts
  dina-font
  (hiPrio proggyfonts)
  uw-ttyp0
  tamzen
  powerline-fonts
  # not sure this one belongs here :shrug:
  fontforge-gtk


#}
# extras {
]; extras = with def; [

  sl                        # to fix ls typos, of course
#Bbsod
  pipes                # wine but better XD
  vitetris                  # epic terminal-based tetris game
#Belectricsheep             # A p2p artwork creating screensaver
  breeze-icons              # icon theme
  libsForQt5.qtstyleplugins # let QT themes apply properly
  adwaita-qt                # blend QT into gnome's style
  qt5ct                     # theming for stuff inside xorg
  xsettingsd                # application settings, alternative to gsettingsd
  lxappearance              # theming
  touchegg                  # touch gestures


#}
# vm {
]; vm = with def; [

  dolphinEmuMaster          # wii/gamecube emulator (near-git version)
  dolphinEmuMaster          # wii/gamecube emulator (near-git version)
  rpcs3                     # ps3 emulator
#Banbox                     # Android APK runner
# virt-manager              # VM manager
  virtualbox                # VM host
  qemu-utils                # qemu tools
  (hiPrio qemu)         # VM method
  qemu_kvm                  # VM qumu bridge
# kvm                       # VM method
#Blibguestfs                # for accessing guest fs's
  # IDK, kvm stuff...
  bridge-utils
# virt-viewer
  dnsmasq
  vde2
  netcat
  edk2
  OVMF
# OVMFFull
  libvirt
  virtlyst
# swtpm
# swtpm-tpm2


#}
# xorg {
]; xorg = with def.xorg; [

  u1.autorandr              # display manager
  u1.xdotool                # virtual keypress util
  libXrandr                 # res change lib
  xrandr                    # change your resolution
  u1.wmctrl                 # window manager functions
  xprop                     # x properties
  xwininfo                  # like xprop
  xev                       # input tester
  u1.greetd.greetd          # greeter!
  u1.greetd.tuigreet        # greeter!
  u1.greetd.dlm             # greeter!
  u1.greetd.gtkgreet        # greeter!


#}
# hacking {
]; hacking = with def; [

# keybase kbfs keybase-gui  # less secure, but screw it
# torbrowser                # heck yeah
  cutter                    # cut TCP/IP connections
  wireshark                 # monitor network packages


#}
# flossGames {
]; flossGames = with s2; [

  superTuxKart              # super-tux-kart!
#Bxonotic                   # gud FPS
  _2048-in-terminal
  _0verkill
  cuyo
  #(lib.hiPrio u1.dolphin-emu-primehack)


#}
];
# experimental things {
experimental = with def; [
  dpkg                      # debian
  apt                       # ubuntu
  pacman                    # arch
  libdnf                    # redhat (rpm)
  ];
#}

# gpu wrappers {
	gpu = with nixGL; [
	nixGLIntel
	auto.nixGLDefault
	auto.nixGLNvidia
];
#}


# guinea {
	guinea = with u1; [
  snowflake                 # anti-censorship?
];
#}


 packages = with u1; {

# haskell {
# use ghcWithHoogle rather than ghcWithPackages, this way you get nice haddock
# pages, and a localhost hoogle instance!
haskell =
  let f = haskell.lib.appendConfigureFlag;
 #let f = p: flags: haskell.lib.appendConfigureFlag p (map (txt: "-f "+txt) flags);
  in haskellPackages.ghcWithHoogle (p: with p; [
# s1.wayland s1.wayland-scanner
# inputs.waymonad           # experimental WM
  xmonad                    # wm
  xmonad-contrib            # xmonad stuff
# xmonad-extras             # xmonad-contrib++ # needs transition to flakes
  xmonad-utils              # xmonad more more stuff stuff
# DescriptiveKeys           # xmonad stuff more in the more of the stuff
f xmobar "-f all_extensions"# xmobar addition for xmonad
# control-bool

  random-fu                 # functions for use in xmonad
#?text                      # useful functions for working w/ json

  yi-custom                 # haskellEditor

# yi-custom.yi-core
# yi-custom.yi-frontend-pango
# yi-custom.yi-frontend-vty
# yi-custom.yi-fuzzy-open
# yi-custom.yi-ireader
# yi-custom.yi-keymap-cua
# yi-custom.yi-keymap-emacs
# yi-custom.yi-keymap-vim
# yi-custom.yi-language
# yi-custom.yi-misc-modes
# yi-custom.yi-mode-haskell
# yi-custom.yi-mode-javascript
# yi-custom.yi-snippet
# yi-custom.yi-rope


]);
#}
# vim {
vim = with def.vimPlugins; [
  (hiPrio haskell-vim)  # vim.hs
  vim-fish                  # vim.fish
];
#}

};
media = {

# managing {
managing = with def; [
  alsa-firmware             # firmware for soundcards
  pulseaudio                # I really hate typing that, but alsa just doesn't work
  pavucontrol               # pulseaudio manager
  playerctl                 # manage media easier
  easyeffects               # apply effects to audio in pw
  pipewire wireplumber      # pipewire!
  qpwgraph helvum carla     # pipewire, testing stuff
];
#}
# capturing {
capturing = with def; [
  maim                      # screenshot tool
  scrot                     # super simple screenshot utility
  flameshot                 # screenshot utility
  capture                   # A "no BS" CLI simple screen capture utility
  obs-studio                # OBS itself
# obs-v4l2sink              # OBS virtual camera - merged into OBS
# obs-move-transition       # OBS move items upon transition
# obs-wlrobs                # OBS wayland screen captures
];
#}
# creation {
creation = with def; [
#Bblender                   # 3d editor ftw
  cura                      # 3d slicer
  avidemux                  # a very basic video editor
# olive-editor              # a more complex video editor
  kdenlive                  # a more complex video editor
#tardour                    # music.mp3
  audacity                  # modify.mp3
  gimp                      # modify.img
  krita                     # paint.img
  akira-unstable            # SVGs.img
];
#}
# viewing {
viewing = with def; [
#+lbry.drv
  celluloid mpv vlc         # multimedia players
#Bnuclear                   # music streamer
  minitube yt-dlp           # view youtube videos
#Bnewsflash                 # RSS reader
  gpodder vocal             # podcast clients
#Tcpod                      # an electron podcast client in need of some love
];
#}


};
utils = {

# terminals {
terminals = with def; [
  st                        # my terminals
#Balacritty                 # my terminals
#Btermite                   # my terminals
];
#}
# debuggers {
debuggers = with def; [
  cgdb gdb pwndbg edb       # general debuggers suite :)
  bench                     # CLI benchmarking tool
# pydb                      # python debugger
  bashdb                    # bash debugger
  delve                     # golang debugger
];
#}
# security {
security = with def; [
  # open VPN solutions
  openvpn gnome3.networkmanager-openvpn
  # proton vpn solution
  protonvpn-cli #protonvpn-gui
  # wireguard vpn stuff
  boringtun wg-bond wireguard-tools
  firejail                  # sandbox applications, particularly useful with web browsers
  usbguard                  # anti badusb
  tor torsocks              # online protection
  macchanger                # randomize your mac address
  s1.vulnix                 # vuln scanner
  sshguard                  # anti ssh attacks
  ferm                      # complex firewall management
  firehol                   # "a firewall for humans"
  opensnitch
  opensnitch-ui             # another firewall??
];
#}
# shells {
shells = with def; [
  any-nix-shell             # connect nix-shell with yours
  zsh                       # Zsh because it's useful I guess...
  bash bashInteractive      # Bash shell for easier scripting
];
#}
# cli {
cli = with def; [
  cointop                   # crypto tracker
#Mstonks                    # stocks tracker - in Arch Repos only, atm
  bottom htop               # system monitors
  ftop                      # file-top
  nix-top                   # nix watcher

  hostname                  # the ability to modify your hostname | un-nix?
  bat exa fd                # better coreutils in rust
  lzip gzip bzip2 xz        # zippers
  curl wget                 # downloaders
  more less                 # pagers
#Bdoas                      # BSD.sudo (Linux port)
  git git-annex             # version control + file management solution
#~ckb-next                  # manage corsair products
  wally-cli                 # flash your epic keyboard
  qmk                       # qmk firmware management
  archivemount              # mount archives (via FUSE)
  flootty                   # collab TTY
  xclip                     # clipboard util
  rsync                     # powerful file copy/share
  ffsend                    # file share for use with FF
  fzf                       # fuzzy finder tool
#Bsnapper snapper-gui       # btrfs snapshot management utility
  btrfs-progs               # betterfs tools
  screen                    # multiplex/session manager
  neofetch pfetch           # show off your system
  remake                    # better verson of GNU make
  pandoc                    # conversion tool
];
#}
# gui {
gui = with def; [
  xdragon                   # drag-n-drop utility
  czkawka                   # file duplicate management tool
  transmission-gtk          # torrent tool! also has cli
  sxiv                      # simple x image viewer
  clipit                    # clipboard manager
# parcellite                # clipboard manager
  xzoom                     # screen magnifier   : These work well
  colorpicker               # hex value of pixel : together.
  brightnessctl             # manage brightness
# remmina                   # remote connection client
  cinnamon.warpinator       # LAN file sharer
  xfce.thunar gvfs          # file manager + trash support
  conky                     # snazzy
  deadd-notification-center # notification system
  gxmessage                 # dialog system
  caffeine-ng               # stop screen-locking
  blueman                   # bluetooth tray
# (hiPrio blueman)      # bluetooth tray
  networkmanagerapplet      # network tray
  stalonetray               # better system tray
# trayer                    # system tray
  mate.engrampa             # Archive tool GUI
# ark                       # Archive tool GUI
  s1.qalculate-gtk          # nice calculator
  kmplot                    # plotting software (graphing calc)
  tilem                     # TI# calculator emulator (needs ROM file)
  usermount                 # automatically mount drives as they appear
  gnome.dconf-editor        # dconf editor, duh
  drawing                   # basic drawing tool - like G-drawings
  drawio                    # flowcharts!!!!
  weston                    # basic wayland compositor, for mucking and dev
];
#}


};


#}
# selections {
selections = builtins.concatLists [
  # things that need high package priority to work:
  (map hiPrio [
#  packages.haskell
  ])

  # NixOS only:
  (map ifNix [
  kernel
  packages.python
# experimental  # other distro's package managers
  ])

  custom
  files

  [packages.haskell]

  # Essentials
  unfree
  core # source compiles won't work without it
  etc
  xorg
  fonts

  # Likely desired
  extras
  code
  editors
  hacking
  vm
  gpu

  flossGames

  guinea # pig


  media.managing
  media.creation
  media.capturing
  media.viewing

  packages.vim

  utils.terminals
  utils.debuggers
  utils.security
  utils.shells
  utils.cli
  utils.gui
  # temporary orphans
  [
  #(nur.repos.mic92.hello-nur) # pkgs)
  ]
];
#}
in
{ # main
	options.profiles.dev.enable =
		mkOption {
			description = "enable profile";
			type = with types; bool;
			default = true;
		};
	config = {
		# makes themes, and other things work better off of nixOS
		targets.genericLinux.enable = ! nixos;
		# theming {
		xsession.windowManager.command = "${config.home.profileDirectory}/scripts/wm";
		xsession.enable = true;
		xsession.numlock.enable = true;
		# gtk
		gtk = {
			enable = true;
			theme.name = "Equilux"; #"Sweet-mars"; # "Equilux";
			theme.package = def.equilux-theme; #.sweet; #.equilux-theme;
			iconTheme.package = def.gnome3.gnome-themes-extra;
			iconTheme.name = "Adwaita";
		};
		# qt
		#qt.enable = true;
		#qt.platformTheme = "gtk";
		#}
		# dconf {
		
		# }
		fonts.fontconfig.enable = true;
		systemd.user.services = {
			opensnitch = {
				Unit = {
					Description = "Opensnitch Application Firewall Daemon";
					After = ["network.target"]; 
					PartOf = ["multi-user.target"];
				};
				Service = {
					Type = "simple";
					PermissionsStartOnly = true;
					ExecStartPre = "${def.coreutils}/bin/mkdir -p /etc/opensnitch/rules"; 
					ExecStart = "${def.opensnitch}/bin/opensnitchd -rules-path /etc/opensnitch/rules"; 
					Restart = "always";
					RestartSec = 30;
				};
			};
		};
		services = {
			#udiskie = {
			#	enable = true;
			#	automount = true;
			#	notify = true;
			#	tray = "always";
			#};
			# caffeine-ng used instead
			#caffeine.enable = false;
			#pasystray.enable = true;
			#blueman.enable = true;
			#blueman-applet.enable = true;
			#screen-locker = {
			#	enable = true;
			#	lockCmd = "${def.xsecurelock}/bin/xsecurelock";
			#	inactiveInterval = 1;
			#};
		};
#		programs = {
#			home-manager.enable = true;
#		};
		home = {
			# tracked files {
				file = lists.foldl (a: b: a // b) {} (
					# config/dot/ -> ~/.config
					map dot-config [
						"deadd/"
					] ++
					# config/     -> ~/
					map basic-conf [
						".vimrc"
						".profile"
						".path.fish"
						".path.sh"
					]
				);
#				xresources = {
#					source = ./.Xresources;
#					target = "./.Xresources";
#				};
			#}
			# env vars {
			sessionVariables = {
				EDITOR = "nvim"; # custom script pointing to nvim
				BROWSER = "re.sonny.Junction"; # not in nix repos yet :/
				PAGER = "nvimpager";
				XDG_DATA_DIRS = concatStringsSep ":" (map (profile: "${profile}/share") [ "/nix/var/nix/profiles/default" config.home.profileDirectory "/usr"]);
				QT_QPA_PLATFORMTHEME = "qt5ct";
				NUX_EXCEPTION = "/scripts"; # passed to grep
			};
			sessionPath = [
			"~/.local/bin"
			"~/appimages"
			"${config.home.profileDirectory}/scripts"
			];
			#}
			# install manpages for packages too
			extraOutputsToInstall = ["doc" "info" "devdoc"];
			# apply | selections
			packages = (map hiPrio [ def.nixUnstable def.home-manager ]  ) ++ selections;
			# cursor theme
			pointerCursor = {
				x11.enable       = true;
				package          = def.phinger-cursors;
				#efaultCursor    = "left_ptr";
				name             = "phinger-cursors";
				size             = 16;
			};
		};
	};
}


# things to look into {
# # generate emulator config
#androidenv.emulateApp {
#  name = "emulate-MyAndroidApp";
#  platformVersion = "28";
#  abiVersion = "x86"; # armeabi-v7a, mips, x86_64
#  systemImageType = "google_apis_playstore";
#}
#`environment.etc.nixpkgs.source = inputs.nixpkgs;` to link the flake's nixpkgs to `/etc/nixpkgs`, and add `nixpkgs=/etc/nixpkgs` to `nix.nixPath`
#
#if you want something approximating environment.etc.<name>.source under Home Manager, you could use xdg.dataFile.<name>.source
#then you could use ~/.local/share/nixpkgs
#services.printing.drivers = with pkgs; [ gutenprint gutenprintBin epson-201106w epson-alc1100 epson-escpr epson-escpr2 epson-workforce-635-nx625-series epson_201207w ]
#}
