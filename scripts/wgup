#!/bin/bash
set -ex

ETH=enp5s0
WIF=wlo1
VPN=wg0
[[ $UID != 0 ]] && exec sudo -E "$(readlink -f "$0")" "$@"

up() {
    killall wpa_supplicant dhcpcd || true
    ip netns add physical
    ip -n physical link add $VPN type wireguard
    ip -n physical link set $VPN netns 1
    wg setconf $VPN /etc/wireguard/$VPN.conf
    ip addr add 192.168.4.33/32 dev $VPN
    ip link set $ETH down
    ip link set $WIF down
    ip link set $ETH netns physical
    iw phy phy0 set netns name physical
    ip netns exec physical dhcpcd -b $ETH
    ip netns exec physical dhcpcd -b $WIF
    ip netns exec physical wpa_supplicant -B -c/etc/wpa_supplicant/wpa_supplicant-$WIF.conf -i$WIF
    ip link set $VPN up
    ip route add default dev $VPN
}

down() {
    killall wpa_supplicant dhcpcd || true
    ip -n physical link set $ETH down
    ip -n physical link set $WIF down
    ip -n physical link set $ETH netns 1
    ip netns exec physical iw phy phy0 set netns 1
    ip link del $VPN
    ip netns del physical
    dhcpcd -b $ETH
    dhcpcd -b $WIF
    wpa_supplicant -B -c/etc/wpa_supplicant/wpa_supplicant-$WIF.conf -i$WIF
}

execi() {
    exec ip netns exec physical sudo -E -u \#${SUDO_UID:-$(id -u)} -g \#${SUDO_GID:-$(id -g)} -- "$@"
}

command="$1"
shift

case "$command" in
    up) up "$@" ;;
    down) down "$@" ;;
    exec) execi "$@" ;;
    *) echo "Usage: $0 up|down|exec" >&2; exit 1 ;;
esac
