# My Nix Config

## Disclaimer

My commit messages are created by [build.sh](./build.sh). I will not often
customize these. I will, however, squash commits occasionally.

If you wish to understand the difference between any particular version, read my
source changes, and the comments therein.

You will be able to see every little tweak I make. Well, provided I didn't
squash the commits during that time frame.

### Why?

Nix Flakes uses a git-based system. So, I figured - embrace it! Every time I
wish to try out a new change:  
I open NeoVim, hit `,.`, type [`.nix/.nix`](./.nix)
tap `enter`, `,c`, `C-o`, and wait for my changes to proliferate.

[Build.sh](./build.sh) handles it all. It auto-commits, runs the build command,
and then the switch command.

#### Why (again)?

Yes, it's tedious at times, but it's very much worth it!  
Though, I would like a quicker solution to iterate on home-manager tracked
files. (If you know of one, submit a PR, please 'n thanks!)

---

## Arch + Nix (btw)

- XMonad

- XMobar

- Deadd-notification-center

- nVim

- Wireguard

- Librewolf - main, Firefox, secondary, Brave, tertiary

- Element/Matrix for chatting, and the occasional Discord

- Many others, see [/.nix](./.nix#L22)

## Some things to note

- I customize the \*\*\*\* out of everything, when I feel like it.

- My nix-store is ~22GBs
