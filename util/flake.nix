{
  inputs.git-ignore-nix.url = github:hercules-ci/gitignore.nix/master;

  # Name | Path | Builder | Dependencies
  outputs = { self, nixpkgs, flake-utils }: with nixpkgs.builtins; with nixpkgs.lib; rec {

  inherit head;

  # general {
  concat   = lists.foldl join {};
  join     = a: b: a // b; # equivalent to (//)
  flip     = trivial.flip;
  # }
  # flipN (N:1 -> 1:N) {
  # lipfN (1:N -> N:1) seems impossible...
  flipN = n: f: a: (if n>1 then flipN (n -1) else x: x) (flip f a);
   ## haskell pattern matching version (invalid types...)
   #flipN n f a | n < 2 = f a;
   #            | _     = flipN (n -1) $ flip f a;
 ## static definitions
 #flip2    = f: a: flip  (flip f a);
 #flip3    = f: a: flip2 (flip f a);
  # }
  # packaging {
  overlay = set: use: s: _: src: {
   "${set}" = src."${set}".override (old: {
     overrides = src.lib.composeExtensions (old.overrides or (_: _: {}))
     (use s);
   });
  };
  package = src: use: s: attrsets.mapAttrsToList (_: v: v ) (use s src src);
  using   = s:   src: _: attrsets.mapAttrs       (n: v: v src n) s;
  # }

  # overlay "haskellPackages" using {  yi = simple ./yi "haskellPackages" "callCabal2nix";  };
  # package nixpkgs           using {  yi = simple ./yi "haskellPackages" "callCabal2nix";  };

  # builders { 
  simple = p: b: pkgs:
    flipN 2 pkgs."${b}" (git-ignore-nix.lib.gitignoreSource p) { };
  derive = p: b: d: pkgs: n:
    with pkgs; stdenv.mkDerivation {
      name = n;
      src = p;
      nativeBuildInputs = d;
      buildPhase = b;
    }
  haskell = p: b: dev: pkgs: n:
    pkgs.haskellPackages.developPackage {
      inherit dev;
      name = n;
      root = p;
      modifier = # use partial application (needs 1 more arg)
        pkgs.haskell.lib.compose.addBuildTools (with pkgs.haskellPackages; pkgs.lib.optional dev ([
          cabal-install stack
        ] ++ d));
    }
  # }

}

}
