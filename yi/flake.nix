{
	# use nixpkgs revision with the last nonbroken build of yi-core
	inputs.nixpkgs.url = "nixpkgs/0026c79c5509d9d170e85cab71ed7c256b4b0c7b";
	inputs.utils.url = "gitlab:mikenrafter/utils.nix/main";
	
	outputs = { self, nixpkgs, flake-utils, utils }:
	with utils; let

		# simple configuration {
		set   = "haskellPackages";
		build = dev: {
			yi-custom = haskell ./yi-config [] dev;
		};
		# }

		built = standardize self build using nixpkgs;
	in built # // {
#		packages=built.packages
#		// (with nixpkgs.haskellPackages; { inherit
#			yi-core
#			yi-frontend-pango
#			yi-frontend-vty
#			yi-fuzzy-open
#			yi-ireader
#			yi-keymap-cua
#			yi-keymap-emacs
#			yi-keymap-vim
#			yi-language
#			yi-misc-modes
#			yi-mode-haskell
#			yi-mode-javascript
#			yi-snippet
#			yi-rope
#		;});
#	}
	;
}
