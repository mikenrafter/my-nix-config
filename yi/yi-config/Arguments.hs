{-# LANGUAGE CPP #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveDataTypeable #-}
module Arguments where

import Control.Monad.State.Lazy (when,execStateT)
import Data.List                (intersperse,intercalate)
import Lens.Micro.Platform      ((.=))
import Data.Maybe               (fromMaybe)
import Data.Monoid              ((<>))

--port Options.Applicative

import Yi hiding (option)
import Yi.Config.Simple.Types

import Yi.Buffer.Misc                   (lineMoveRel)
import Yi.Config.Default.HaskellMode    (configureHaskellMode)
import Yi.Config.Default.JavaScriptMode (configureJavaScriptMode)
import Yi.Config.Default.MiscModes      (configureMiscModes)
import Paths_yi_core                    (version)
import Data.Version


import System.Console.CmdArgs    hiding (modes)


import qualified Yi.Keymap.Vim                    as     V


import Yi.Config.Default.Vim (configureVim)
import Yi.Config.Default.Vty (configureVty)
import Yi.Config.Default.Pango (configurePango)

import Yi.Debug (initDebug)


frontends :: [(String, ConfigM ())]
frontends =
  [ ("vty", configureVty)
  , ("pango", configurePango)
  , ("", return ())
  ]

keymaps :: [(String, ConfigM ())]
keymaps =
  [ ("vim", configureVim)
  , ("", return ())
  ]

modes :: [(String, ConfigM ())]
modes =
  [ ("editor", return ())
  , ("pager", return ())
  , ("debug", return ())
  , ("", return ())
  ]

syntaxes :: [(String, Int)]
syntaxes =
 [ ("clever" , 1) -- default
 , ("fast"   , 0)
 , ("precise", 2)
 , ("",        1)
 ]

data CommandLineOptions = CommandLineOptions
  { frontend :: String
  , keymap   :: String
  , mode     :: String
  , syntax   :: String

  , actions  :: String
  , line     :: Int
  , files    :: [String]
  } deriving (Show, Data, Typeable)

clOptions :: CommandLineOptions
clOptions = let
  grab = intercalate " | " . map fst . init

  -- List | Omit | Help
  argue def (o,h) = def &= opt o &= help h
  (~-)  l      h  = argue (l!0) (l!1, h<>": ["<>grab l<>"]")
  (~~)  def    h  = argue def (def,h)
  (!)   l      n  = fst $ l!!n

 in CommandLineOptions
 { frontend = frontends ~- "The frontend to use"
 , keymap   = keymaps   ~- "The keymap to use"
 , mode     = modes     ~- "The mode to use"
 , syntax   = syntaxes  ~- "The syntax mode"

 , actions  = ""        ~~ "Actions to start the editor with"
 , line     = 0         ~~ "Alias for G action in vim: '-l5':'-a 5G'"
 , files    = []        &= args
 }
 &= summary "Yi - an extensible editor written and configured in Haskell"
 &= program ("(static build) v0id - v" <> showVersion version)

argumentative :: ConfigM () -> IO ()
argumentative config = do
  args <- cmdArgs clOptions
  let openFileActions = intersperse (EditorA newTabE) $ map (YiA . openNewFile) $ files args
      moveLineAction  = YiA $ withCurrentBuffer $ lineMoveRel $ line args
      actionActions   = moveLineAction -- V.pureEval this $ actions args -- TODO: fill out
  cfg <- (flip execStateT defaultConfig $ runConfigM $ apply args
      >> (startActionsA .= (openFileActions ++ [{-actionActions,-}moveLineAction]))
      >> config)
  when (mode args == "debug") $ initDebug "yi.log"
  startEditor cfg Nothing
--where
-- 

-- apply command-line switches
apply :: CommandLineOptions -> ConfigM ()
apply opts = do
  -- lookup names for [frontends,keymaps,modes] - if element isn't there: no-op
  let obtain  l = fromMaybe (return ())    . flip lookup l
      obtain' l = fromMaybe (fst $ head l) . flip lookup l
  obtain frontends $ frontend opts
  obtain keymaps   $ keymap   opts
  obtain modes     $ mode     opts
  -- TODO integrate modes
  -- setVar "syntaxes" $ obtain' syntaxes     $ syntax opts

