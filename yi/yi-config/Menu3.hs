{-# LANGUAGE OverloadedStrings, BlockArguments, LambdaCase, FlexibleContexts #-}
{-# LANGUAGE TupleSections #-}
module Menu3 where

--{- standard external
import qualified Data.Text                            as T
import qualified Text.ParserCombinators.Parsec        as P
import           Control.Monad.State.Lazy
import           Lens.Micro.Platform
import           System.Environment
import           Data.Prototype
import           Data.Monoid
import           Data.Maybe
import           Data.List
--}

--{- yi *
import           Yi
import           Yi.Types
import           Yi.Config.Simple.Types
import           Yi.Config
import           Yi.Event
import           Yi.String                               (mapLines)
--port           Yi.Boot                          --     (configMain, reload)
import           Yi.Rope                                 (toText,toString,fromString)
--}
import Yi.Core
import Yi.File
import Yi.MiniBuffer (spawnMinibufferE)

import Control.Monad -- (fmap, void)
import Data.Char     -- (isUpper, toLower)

import Utility

-- | Menu
--type Menu      = [Item]
--type Item      = (String,Generator)
--type Generator = MenuContext -> GenM
--type GenM      = EditorM Menu
---- due to frequent usage, lets simplify this...
--type E         = EditorM ()
--pe Menu = [Item]
--ta Item = 

-- | Menu action context
newtype MenuContext = MenuContext
  { buffer :: BufferRef
--  parent :: Generator
  }

{-
draw [] = noop
draw is = showMenu $ mapOver is
 -}


-- | Action on item
--action_ :: (YiAction a x, Show x) => String -> a -> Item
action_ :: (YiAction a x, Show x) => String -> a -> (String,b)
action_ title = action title . const

-- | Action on item with context
--action :: (YiAction a x, Show x) => String -> (MenuContext -> a) -> Item
action :: (YiAction a x, Show x) => String -> (MenuContext -> a) -> (String,b)
action title act = (title,) \ctx -> do
  withEditor closeBufferAndWindowE
  runAction $ makeAction $ act ctx
  return []


--menu   :: String -> Menu      -> Item
--menu   :: String -> [(String,b)]      -> (String,b)
menu a = menu' a . const return
--menu'  :: String -> Generator -> Item
menu'  = (,)


--startMenu :: Key -> Item -> E
startMenu :: Key -> (String,a) -> EditorM ()
startMenu ekey (_,generator) =
  void $ gets currentBuffer >>= startMenu' . MenuContext where
   startMenu' ctx = generator ctx >>= display where
{- back how it was
--    display :: Menu -> E
    display []    = yield
    display items = void $ spawnMinibufferE titles $ const
                         $ keys mappings actions where
      titles    = T.pack $ intercalate " | " $ map snd mappings
      mappings  = enkey' $ map fst items
      actions   = map  (($ctx) . snd) items
--}
--{- is it beautiful, or hideous? who's to say...
    display = \case
      []         ->  yield
      items      ->   void $ spawnMinibufferE titles
                           $ keys mappings actions where
         titles   = T.pack $ intercalate " | " $ map snd mappings
         mappings = enkey' $ map fst items
         actions  = map  (($ctx) . snd) items
--      :: Menu    -> E
--}

--  keys :: [(Char,String)] -> [MenuItem] -> KeymapEndo
--  keys :: [(Char,String)] -> [t -> b -> EditorM [(String,a)]] -> KeymapEndo
    keys mappings actions =
      choice $ (map ($cda) [regen,close,toggle]) : ca where
        cda = zipWith cast23 mappings actions -- [(Char,String,GenM)]
        ca  = actions `to` mappings
        to' a (c,_) = spec (KASCII c) ?>>! (recurse $ a ctx)
        to          = zipWith to'

    close    _ = spec KEsc ?>>! kill
    toggle   _ = spec ekey ?>>! kill
    draw    [] = yield
    draw    is = recurse $ return is
    draw'   [] = yield
    draw'   is = recurse $ return is
    regen   is = spec (KASCII '-')  ?>>! kill' $ draw $ map dropFst $ filter ((=='-') . fstOf3) is

    kill       = closeBufferAndWindowE >> return []
    kill'    f = closeBufferAndWindowE >> f >> return []
    recurse is = kill' $ startMenu ekey ("",is)

    yield      = return []
