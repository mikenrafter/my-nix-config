module Menus (
    mainMenu,
    windowsMenu,
    buffersMenu,
    tabsMenu,
    ghciMenu
    ) where

import Yi.Command
import Yi.Core
import Yi.File
import Yi.Keymap.Emacs.Utils (askQuitEditor)
import Yi.Mode.Haskell (ghciLoadBuffer, ghciInferType, ghciSend)
--port Yi.Mode.Haskell.HLint
import Yi.TextCompletion

import Yi.Editor -- (EditorM, MonadEditor, withEditor, closeBufferAndWindowE, withCurrentBuffer, deleteTabE, newTabE)
import Yi.Keymap (YiM, KeymapSet)

import           Yi.Types
import           Yi.Config.Simple.Types
import           Yi.Config

import Menu3

-- | Main menu
mainMenu :: Menu
mainMenu =
  [ menu "file"
    [ action_ "quit" askQuitEditor
    , action  "save" $ fwriteBufferE . buffer
    ]
  , menu "edit"
    [ action_ "auto complete" wordComplete
    --action_ "completion" completeWordB
    --action_ "sort lines" sortLines
    ]
  , menu "tools"
    [ menu "ghci" ghciMenu
--  , action_ "Hlint" hlint
    , action_ "shell command" shellCommandE
    ]
  , menu "view"
    [ menu "windows" windowsMenu
    , menu "tabs" tabsMenu
    , menu "buffers" buffersMenu
    , menu "layout"
      [ action_ "next" layoutManagersNextE
      , action_ "previous" layoutManagersPreviousE
      ]
    ]
  , menu "settings"
    [--ction_ "set indent" setIndent
    ]
  ]

-- | Windows menu
windowsMenu :: Menu
windowsMenu =
  [ action_ "next" nextWinE
  , action_ "previous" prevWinE
  , action_ "split" splitE
  , action_ "swap" swapWinWithFirstE
  , action_ "close" tryCloseE
  , action_ "close-others" closeOtherE
  ]

-- | Buffers menu
buffersMenu :: Menu
buffersMenu =
  [--ction_ "next" nextBufW
  --action_ "previous" prevBufW
    action_ "close" closeBufferAndWindowE
  --action_ "show all" openAllBuffersE
  ]

-- | Tabs menu
tabsMenu :: Menu
tabsMenu =
  [ action_ "next" nextTabE
  , action_ "previous" previousTabE
  , action_ "new" newTabE
  , action_ "delete" deleteTabE
  ]

-- | GHCi menu
ghciMenu :: Menu
ghciMenu =
  [ action_ "load" ghciLoad
  , action_ "infer-type" ghciInfer
  ]

-- | Load buffer in GHCi
ghciLoad :: YiM ()
ghciLoad = do
  ghciLoadBuffer
  ghciSend $ ":set prompt " ++ show "ghci> "

-- | Infer type
ghciInfer :: YiM ()
ghciInfer = do
  ghciLoad
  ghciInferType

{- | Set default indentation size to 4
setIndent :: BufferM ()
setIndent = modifyMode $ modeIndentSettingsA ^: modifyIndent where
    modifyIndent is = is {
        tabSize = 4 }
--}
