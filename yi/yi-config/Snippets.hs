{-# LANGUAGE OverloadedStrings #-}
module Snippets
    ( snips
    ) where

import Data.Char (isUpper)
import Data.Monoid
import Control.Monad (void)
import System.FilePath
import qualified Data.Text as T

import qualified Yi.Rope as R
import Yi.Snippet
import Data.Maybe

import Utility

snips :: [Snippet]
snips
  =  derivings
  <> comments
  <> imports
  <> pragmas
  <> codes

codes :: [Snippet]
codes =
  [ Snippet "m" $ do
      moduleName <- guessModuleName <$> refer filename
      line ("module " <> moduleName <> " where")
  , Snippet "un" $ lit "undefined"
  , Snippet "printIO" $ lit "liftIO . print $"
  , Snippet "f"  $ void (lit "(\\ " *> place "_" <* lit " -> " <* place "undefined" <* lit ")" <* nl)
  , Snippet "ff" $ void (lit "$ \\ " *> place "_" <* lit " -> " <* place "undefined" <* nl)
  , Snippet "if" $ void $ do
      lit "if "   *> place "" <* nl
      lit "then " *> place "" <* nl
      lit "else " *> place "" <* nl
  ]

pragmas :: [Snippet]
pragmas =
  -- language pragmas
  [ Snippet "pra"    $ void $ lit "{-# LANGUAGE " *> place "OverloadedStrings" <* lit " #-}" <* nl
  , Snippet "str"    $ line "{-# LANGUAGE OverloadedStrings #-}"
  , Snippet "gadt"   $ line "{-# LANGUAGE GADTs #-}"
  , Snippet "rank"   $ line "{-# LANGUAGE RankNTypes #-}"
  , Snippet "scoped" $ line "{-# LANGUAGE ScopedTypeVariables #-}"
  , Snippet "ffi"    $ line "{-# LANGUAGE ForeignFunctionInterface #-}"
  , Snippet "syn"    $ line "{-# LANGUAGE TypeSynonymInstances #-}"
  , Snippet "mparam" $ line "{-# LANGUAGE MultiParamTypeClasses #-}"
  , Snippet "bang"   $ line "{-# LANGUAGE BangPatterns #-}"
  , Snippet "sigs"   $ line "{-# LANGUAGE InstanceSigs #-}"
  , Snippet "mono"   $ line "{-# LANGUAGE NoMonomorphismRestriction #-}"
  , Snippet "stand"  $ line "{-# LANGUAGE StandaloneDeriving #-}"
  , Snippet "lambda" $ line "{-# LANGUAGE LambdaCase #-}"
  , Snippet "tuple"  $ line "{-# LANGUAGE TupleSections #-}"
  , Snippet "puns"   $ line "{-# LANGUAGE NamedFieldPuns #-}"
  , Snippet "temp"   $ line "{-# LANGUAGE TemplateHaskell #-}"
  , Snippet "flex"   $ do
      line "{-# LANGUAGE FlexibleInstances #-}"
      line "{-# LANGUAGE FlexibleContexts #-}"
      line "{-# LANGUAGE TypeSynonymInstances #-}"
  , Snippet "gnew"   $ do
      line "{-# LANGUAGE DeriveFunctor #-}"
      line "{-# LANGUAGE GeneralizedNewtypeDeriving #-}"
  , Snippet "constraints" $ do
      line "{-# LANGUAGE ConstraintKinds #-}"
      line "{-# LANGUAGE FlexibleContexts #-}"
  , Snippet "derive" $ do
      line "{-# LANGUAGE DeriveDataTypeable #-}"
      line "{-# LANGUAGE DeriveGeneric #-}"
      line "{-# LANGUAGE DeriveFunctor #-}"
      line "{-# LANGUAGE DeriveTraversable #-}"
      line "{-# LANGUAGE DeriveFoldable #-}"

  -- ghc-options
  , Snippet "opt"    $ line "{-# OPTIONS_GHC ${1} #-}"
  , Snippet "wall"   $ line "{-# OPTIONS_GHC -Wall #-}"
  , Snippet "nowarn" $ do
      line "{-# OPTIONS_GHC -fno-warn-name-shadowing  #-}"
      line "{-# OPTIONS_GHC -fno-warn-type-defaults   #-}"
      line "{-# OPTIONS_GHC -fno-warn-unused-do-bind  #-}"
  ]

imports :: [Snippet]
imports =
  [ Snippet "im" $ void $ lit "import " *> place "Data.Text" <* nl
  , Snippet "iq" $ do
      lit "import qualified "
      moduleName <- place "Data.HashMap.Strict"
      lit " as "
      abbrev <- findAbbrev <$> refer moduleName
      lit abbrev

  , Snippet "mm" $ do
      line "import Control.Monad"
      line "import Control.Applicative"
      line "import Control.Monad.State"
      line "import Control.Monad.Reader"
      line "import Control.Monad.Except"

  , Snippet "usual" $ do
      line "import Data.Maybe"
      line "import Data.Functor"
      line "import Data.Foldable"
      line "import Data.Traversable"
      line "import Control.Monad"
      line "import Control.Applicative"

  , Snippet "fold" $ do
      line "import Data.Foldable"
      line "import Data.Traversable"

  , Snippet "bs" $ do
      line "import qualified Data.ByteString as S"
      line "import qualified Data.ByteString.Char8 as S8"

  , Snippet "containers" $ do
      line "import qualified Data.HashMap.Lazy as HashMap"
      line "import qualified Data.HashSet      as HashSet"
      line "import qualified Data.IntMap       as IntMap"
      line "import qualified Data.IntSet       as IntSet"
      line "import qualified Data.IxSet        as IxSet"
      line "import qualified Data.Map          as Map"
      line "import qualified Data.Sequence     as Seq"
      line "import qualified Data.Set          as Set"
  ]

derivings :: [Snippet]
derivings =
  [ Snippet "dd" $ line "deriving (Eq, Ord, Show)"
  , Snippet "dg" $ line "deriving (Eq, Ord, Show, Typeable, Data, Generic)"
  ]

comments :: [Snippet]
comments =
  [ Snippet "bbar" $ line ("-- "<> R.fromText (T.pack $ replicate 74 '=') <>" --")
  , Snippet "bar"  $ line . R.fromText . T.pack $ replicate 80 '-'
  , Snippet "box"  $ genericbox "(c) Mike Nrafter, 2022" "GPL3"        "mikenrafter@proton.me"
--, Snippet "sbox" $ genericbox "(c) Mike Nrafter, 2022" "Proprietary" "mikenrafter@proton.me"
  ]
  where
    genericbox :: R.YiString -> R.YiString -> R.YiString -> SnippetBody ()
    genericbox c l m = do
      f <- guessModuleName <$> refer filename
      line  "-------------------------------------------------------------------------------"
      line  "-- |"
      line ("-- Module    :  " <> f)
      lit   "-- Copyright :  " *> place c <* nl
      lit   "-- License   :  " *> place l <* nl
      lit   "-- Maintainer:  " *> place m <* nl
      line  "-- Stability :  experimental"
      line  "-- Portability: non-portable"
      line  "--"
      lit   "-- " *> place "" <* nl
      line  "-------------------------------------------------------------------------------"
