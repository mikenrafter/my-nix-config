--nix build . --offline && result/bin/yi #
{-# LANGUAGE OverloadedStrings, LambdaCase, BlockArguments, TupleSections #-}

module Utility where

--{- imports
import Control.Monad -- (void)
import Control.Applicative -- (void)
import Control.Monad.Extra (ifM)
import Control.Monad.State.Lazy (gets, execStateT, get, put)
import Data.Char
import Data.List
import Data.List.Split
import Data.List.PointedList.Circular (insertRight)
import System.Environment
import System.Directory
import Data.Text (pack)
import qualified Data.Text as T
import Control.Concurrent.MVar
import Data.Prototype (override)
import Lens.Micro.Platform ((.=),(%=))
import Prelude hiding (foldl)
import Yi hiding (yellow, blue, green, red, cyan, super)
import Yi.Buffer.Basic (Direction(Forward), Point(..))
import Yi.Buffer.HighLevel (readCurrentWordB)
import Yi.Buffer.Misc (setVisibleSelection)
import Yi.Config (configUI, configWindowFill)
import Yi.Config.Default (defaultConfig)
import Yi.Config.Default.HaskellMode (configureHaskellMode)
import Yi.Config.Default.MiscModes (configureMiscModes)
import Yi.Config.Default.Pango (configurePango)
import Yi.Config.Default.Vim (configureVim)
import Yi.Config.Default.Vty (configureVty)
import Yi.Config.Lens (defaultKmA, configUIA, startActionsA, initialActionsA)
import Yi.Config.Simple.Types (ConfigM, runConfigM)
import Yi.Core (startEditor, refreshEditor)
import Yi.Dired (dired)
import Yi.Editor (EditorM, MonadEditor, withEditor, closeBufferAndWindowE, withCurrentBuffer, refSupply, stringToNewBuffer, tabsA, newWindowE)
import Yi.File (viWrite, fwriteAllY, openNewFile)
import Yi.Keymap (YiM, KeymapSet)
import Yi.Keymap.Emacs.KillRing (killLine)
import Yi.Keymap.Vim (mkKeymapSet, defVimConfig, vimBindings, pureEval)
import Yi.Keymap.Vim.StateUtils (switchModeE, resetCountE, modifyStateE)
import Yi.Keymap.Vim.Utils (mkStringBindingE, mkStringBindingY)
import Yi.Keymap.Vim.Ex.Commands.Registers (printRegisters)
import Yi.Rope (fromString, toString, YiString(..))
import qualified Yi.Rope as R
import Yi.Search (doSearch, SearchOption(IgnoreCase))
import Yi.Tag (TagTable, setTags)
import Yi.Tab (makeTab1)
import Yi.Types (Action(YiA,EditorA))
import Yi.Utils
import Yi.Snippet
import Yi.Process
import Yi.MiniBuffer
import qualified Yi.Buffer.HighLevel as BH
import qualified Yi.Buffer.Misc as B
import           Yi.Buffer.Misc
import qualified Yi.Editor as E
import qualified Yi.Keymap.Vim.Common as V

import System.Process
import System.IO
import System.FilePath

import           System.Locale (defaultTimeLocale)
import           Data.Time.Clock (getCurrentTime)
import           Data.Time.Format (formatTime)

--}

--{- github:aiya000
-- resizeCurrentWin :: Int -> EditorM ()
-- resizeCurrentWin lineNum = undefined
-- normal gd of Vim
searchFromHead :: EditorM ()
searchFromHead = do
  word <- toString <$> withCurrentBuffer readCurrentWordB
  withCurrentBuffer $  moveToLineColB 0 0
  void $ doSearch (Just word) [IgnoreCase] Forward
-- Highlight word
--staySearch :: EditorM ()
staySearch = do
  (x,y) <- withCurrentBuffer BH.getLineAndCol
  word <- toString <$> withCurrentBuffer readCurrentWordB
  void $ doSearch (Just word) [IgnoreCase] Forward
  withCurrentBuffer $ moveToLineColB x y
-- Clone a window to right, this means default behavior of Vim's :vsplit
vsplit :: EditorM ()
vsplit = E.splitE >> E.prevWinE
--}


--{- github:jaredloomis
-- | Set working directory to the directory
--   containing the file.
cdToFile :: FilePath -> IO ()
cdToFile f =
    if '/' `notElem` f
        then return ()
    else setCurrentDirectory $ getFileDir f

getFileDir :: FilePath -> FilePath
getFileDir = (++"/") . intercalate "/" . init . splitOn "/"

-- haskell

-- | Generate a type signature for word under cursor.
--   Only works for identifiers GHCi can see.
--   Inserts type signature on the next line up
--   from the cursor.
genSignatures :: YiM ()
genSignatures = do
    word <- withCurrentBuffer $ readUnitB unitWord
    sigRaw <- getSignature $ toString word
    if not $ null sigRaw
        then withCurrentBuffer $ lineUp >> insertN (fromString $ '\n':sigRaw)
    else
        withMinibufferFree (pack ("Cannot detect type of \"" ++ (toString word) ++ "\"."))
                           (const $ return ())

-- | Get the type of word under cursor and
--   display on Minibuffer.
miniSignature :: YiM ()
miniSignature = do
    word <- withCurrentBuffer $ readUnitB unitWord
    sigRaw <- getSignature $ toString word
    when (not $ null sigRaw) $
        withMinibufferFree (pack sigRaw) (const $ return ())

-- | Given a String global identifier, ask
--   GHCi to tell us the type signature.
--   If GHCi doesn't know, "" is returned.
getSignature :: String -> YiM String
getSignature word = do
    (Just stdin', Just stdout', Just stderr', handle) <-
        ghciProcess >>= io . createProcess
    io $ hPutStrLn stdin' $ ":t " ++ word
    ghciOut <- io $ hGetContents stdout'
    ghciErr <- io $ hGetContents stderr'

    let ghciLines = lines ghciOut
    if "parse error on input" `isInfixOf` ghciErr || not (length ghciLines >= 2)
        then return ""
    else do
        let relevantLine = ghciLines !! (length ghciLines - 2)
            parsedLine = tail $ dropWhile (/=' ') relevantLine
        return parsedLine

ghciProcess :: YiM CreateProcess
ghciProcess =
  withCurrentBuffer (gets file) >>= \case
    Nothing -> error "yi.hs: ghciProcess"
    Just name -> return $
      CreateProcess
        { cmdspec            = (RawCommand "ghci" [name])
        , cwd                = Nothing
        , env                = Nothing
        , std_in             = CreatePipe
        , std_out            = CreatePipe
        , std_err            = CreatePipe
        , close_fds          = False
        , create_group       = False
        , delegate_ctlc      = False
        , detach_console     = False
        , create_new_console = False
        , new_session        = False
        , child_group        = Nothing
        , child_user         = Nothing
        , use_process_jobs   = False
        }

--{- Testing stuff. Changes highlighting of Points 0-1000.
test :: YiString -> EditorM ()
test = withCurrentBuffer . addOverlayB . overlay
overlay :: YiString -> Overlay
overlay s = mkOverlay "UL" (mkRegion (Point 0) (Point 1000)) commentStyle s
--}
--}


-- | Switch to other opened buffer (asks in minibuffer)
switchBuffer :: YiM ()
switchBuffer = promptingForBuffer "buffer name:"
    (withEditor . switchToBufferE)
    (\o a -> (a \\ o) ++ o)


{-faultModeLine :: [T.Text] -> BufferM T.Text
defaultModeLine prefix = do
    col <- curCol
    pos <- pointB
    ln <- curLn
    p <- pointB
    s <- sizeB
    curChar <- readB
    ro <-use readOnlyA
    modeNm <- gets (withMode0 modeName)
    unchanged <- gets isUnchangedBuffer
    let pct
          | pos == 0 || s == 0 = " Top"
          | pos == s = " Bot"
          | otherwise = getPercent p s
        changed = if unchanged then "-" else "*"
        readOnly' = if ro then "%" else changed
        hexxed = T.pack $ showHex (ord curChar) ""
        hexChar = "0x" <> T.justifyRight 2 '0' hexxed
        toT = T.pack . show

    nm <- gets $ shortIdentString (length prefix)
    return $ T.concat [ readOnly', changed, " ", nm
                      , "     ", hexChar, "  "
                      , "L", T.justifyRight 5 ' ' (toT ln)
                      , "  "
                      , "C", T.justifyRight 3 ' ' (toT col)
                      , "  ", pct , "  ", modeNm , "  ", toT $ fromPoint p
                      ]
--}


newEmptyTabE :: EditorM ()
newEmptyTabE = do
  win <- newWindowE False =<< stringToNewBuffer (MemBuffer "<new>") mempty
  ref <- newRef
  tabsA %= insertRight (makeTab1 ref win)

newRef :: EditorM Int
newRef = do
  s <- get
  let n = refSupply s
  put (s { refSupply = n+1 })
  pure (n+1)


guessModuleName :: R.YiString -> R.YiString
guessModuleName
  = R.fromText . T.intercalate "."
  . reverse . takeWhile isCapitalized . reverse
  . T.splitOn "/"
  . T.pack . dropExtension . T.unpack
  . R.toText
  where
    isCapitalized s = case T.uncons s of
      Just (c, _) -> isUpper c
      Nothing -> False

dropCommon :: R.YiString -> R.YiString
dropCommon s =
  case (R.split (== '.') s) of
    [x] -> x
    "Control":rest -> R.intercalate "." rest
    "Data":rest    -> R.intercalate "." rest
    _   -> s


findAbbrev :: R.YiString -> R.YiString
findAbbrev = R.take 2 . R.filter (`elem` ['A'..'Z']) . dropCommon


expander snips = do
 expanded <- expandSnippetE (defEval "<Esc>") snips
 unless expanded (defEval "  ")

defEval   = pureEval (extractValue defVimConfig)



-- Softtabs of 2 characters for Berkeley coding style, if not editing makefile.
prefIndent :: Mode syntax -> Mode syntax
prefIndent m =
  if modeName m == "Makefile"
  then m
  else m {
   modeIndentSettings = IndentSettings
    { expandTabs = True
    , shiftWidth = 2
    , tabSize    = 2
    }
   }




-- inserting current date and underline
currentDate :: IO String
currentDate = do
  getCurrentTime >>= return . show
  ----turn $ formatTime locale  "%A %b %e %Y" tim
  --turn $ show tim
  --ere locale = defaultTimeLocale

makeUnderline :: String -> String
makeUnderline s = s <> ('\n' : line) <> "\n"
  where line = take (length s) $ repeat '='

currentDateAndUnderline :: IO String
currentDateAndUnderline = do
  currentDate >>= return . makeUnderline

insertCurrentDate :: YiM ()
insertCurrentDate =
  withCurrentBuffer . insertN . R.fromString =<< withUI \_ -> currentDateAndUnderline



-- fn [(key, idx)] [element]
-- fn [(Char,Int)] [String ]
--key keys ids values = if length ids < length values
--then 

--{- keying
enkey  :: [String] -> [(Char,String,String)]
enkey  = map encode  . keyItems
enkey' :: [String] -> [(Char,String)]
enkey' = map encode' . keyItems
keyItems :: [String] -> [(Char,Int,String)]
keyItems keys = zipWith (\(c,i) k -> (c,i,k)) (getChars [] [] keys) keys
getChars :: [Int] -> String -> [String] -> [(Char,Int)]
getChars ids chars (k:ks) =
  let x = head $ filter (not . flip elem chars . snd) (zip [0..] k) <> [(-1,'-')]
  in getChars (ids<>[fst x]) (chars<>[snd x]) ks
getChars ids chars []     = zip chars ids
encode   :: (Char,Int,String) -> (Char,String,String)
encode s@(c,_,k) = let x = encode' s in (c, snd x, k)
encode'  :: (Char,Int,String) -> (Char,String)
encode'  (c,i,k) = (c, replace (toUpper c) k i)
replace :: Char -> String -> Int -> String
replace                   = replace' "" where
  replace' p c      []  n = p<>[c]
  replace' p c s@(x:xs) n = case n of
    (-2) -> s
    (-1) -> '-':s
    0    -> p<>(c:xs)
    n    -> replace' (p<>[x]) c xs $ n-1
-- | Title to keymap
existing :: String -> Maybe (Char, String)
existing t =
  \case
   Nothing -> Nothing
   Just c  -> Just (c,t)
  $ fmap toLower $ find isUpper t

--}


--{- clever functions

-- useful for eta reduction
-- f a b c = (a,b c)
-- - becomes -
-- f = ($$ f')
($$) a = ((a,) .)
--fixr 9 $$

--}
--{- tuple manipulation
-- copied from GHC.Utils
fstOf3 (x,_,_) = x
sndOf3 (_,x,_) = x
thdOf3 (_,_,x) = x

cast23  (a,b) c = (a,b,c)
dropFst (_,b,c) = (b,c)
--}
