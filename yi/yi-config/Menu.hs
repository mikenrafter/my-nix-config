{-# LANGUAGE OverloadedStrings, BlockArguments, LambdaCase, FlexibleContexts #-}

module Menu (
    Menu,
    MenuItem,
    MenuContext(..),
    menu, action, action_,
    startMenu
    ) where

--{- standard external
import qualified Data.Text                            as T
import qualified Text.ParserCombinators.Parsec        as P
import           Control.Monad.State.Lazy
import           Lens.Micro.Platform
import           System.Environment
import           Data.Prototype
import           Data.Monoid
import           Data.Maybe
import           Data.List
--}

--{- yi *
import           Yi
import           Yi.Types
import           Yi.Config.Simple.Types
import           Yi.Config
import           Yi.Event
import           Yi.String                               (mapLines)
--port           Yi.Boot                          --     (configMain, reload)
import           Yi.Rope                                 (toText,toString,fromString)
--}
import Yi.Core
import Yi.File
import Yi.MiniBuffer (spawnMinibufferE)

import Control.Monad -- (fmap, void)
import Data.Char     -- (isUpper, toLower)

import Utility

-- | Menu
type Menu = [MenuItem]

-- | Menu item
data MenuItem
  = MenuAction String (MenuContext -> Char -> Keymap)
  | SubMenu    String Menu

-- | Menu action context
newtype MenuContext = MenuContext
  { buffer :: BufferRef
--, parent :: Menu
  }

-- | Sub menu
menu :: String -> Menu -> MenuItem
menu = SubMenu

-- | Action on item
action_ :: (YiAction a x, Show x) => String -> a -> MenuItem
action_ title act = action title $ const act

-- | Action on item with context
action :: (YiAction a x, Show x) => String -> (MenuContext -> a) -> MenuItem
action title act = MenuAction title \ctx c ->
  char c ?>>! do
    withEditor closeBufferAndWindowE
    runAction $ makeAction $ act ctx

-- | Fold menu item
foldItem
    :: (String -> (MenuContext -> Char -> Keymap) -> a)
    -> (String -> [a] -> a)
    -> MenuItem
    -> a
foldItem mA sM (MenuAction title act) = mA title act
foldItem mA sM (SubMenu    title  sm) = sM title $ map (foldItem mA sM) sm

-- | Fold menu
foldMenu
    :: (String -> (MenuContext -> Char -> Keymap) -> a)
    -> (String -> [a] -> a)
    -> Menu
    -> [a]
-- menu action, submenu -> menu -> actions
foldMenu mA = map . foldItem mA

-- | Menu title to keymap
menuEvent :: String -> Maybe (Char, String)
menuEvent t =
  \case
   Nothing -> Nothing
   Just c -> Just (c,t)
  $ toLower <$> find isUpper t

-- | Start menu action
startMenu :: Key -> Menu -> EditorM ()
startMenu ekey m = do
  ctx <- MenuContext <$> gets currentBuffer
  startMenu' ctx m
  where
    startMenu' ctx = showMenu . foldMenu onItem onSub where
      --{- display
      showMenu :: [(String, Maybe Keymap)] -> EditorM ()
      showMenu is = void $ spawnMinibufferE menuItems $ const $ subMap is where
        menuItems = T.pack $ intercalate " | " $ map fst is
      --}
      --{- handling
      onItem title act = (snd kt, fmap (act ctx) $ Just $ fst kt) where
        kt = key title
      onSub title is   = (snd kt, fmap subMenu $ Just $ fst kt) where
        subMenu c = char c ?>>! closeBufferAndWindowE >> showMenu is
        kt = key title
      subMap is = choice $ close : toggle : mapMaybe snd is
      --}
      --{- keys/generation
      close  = spec KEsc ?>>! closeBufferAndWindowE
      toggle = spec ekey ?>>! closeBufferAndWindowE
      key title = fromMaybe (head $ enkey' [title]) $ menuEvent title
      --}
