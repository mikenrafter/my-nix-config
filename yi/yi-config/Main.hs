--nix build . --offline && result/bin/yi #
{-# LANGUAGE OverloadedStrings, BlockArguments, LambdaCase #-}

--{- imports
--{- standard external
import qualified Data.Text                            as T
import qualified Text.ParserCombinators.Parsec        as P
import           Control.Monad.State.Lazy
import           Lens.Micro.Platform
import           System.Environment
import           Data.Prototype
import           Data.Monoid
import           Data.Maybe
import           Data.List
--}
--{- yi *
import           Yi
import           Yi.Types
import           Yi.Config.Simple.Types
import           Yi.Config
import           Yi.Event
import           Yi.String                               (mapLines)
--port           Yi.Boot                          --     (configMain, reload)
import           Yi.Rope                                 (toText,toString,fromString)
--}
--{- config
import           Yi.MiniBuffer                           (spawnMinibufferE)
import           Yi.Config.Default.Vty
import           Yi.Config.Default                       (defaultConfig)
import           Yi.Config.Default.HaskellMode           (configureHaskellMode)
import           Yi.Config.Default.JavaScriptMode        (configureJavaScriptMode)
import           Yi.Config.Default.MiscModes             (configureMiscModes)
import           Yi.Config.Simple                 hiding (super)
--{- vim
import           Yi.Config.Default.Vim
import qualified Yi.Keymap.Vim                    as     V
import qualified Yi.Keymap.Vim.Common             as     V
import qualified Yi.Keymap.Vim.Ex.Types           as     V
import qualified Yi.Keymap.Vim.Ex.Commands.Common as     V
import qualified Yi.Keymap.Vim.Utils              as     V
import           Yi.Keymap.Vim.StateUtils               --switchModeE, resetCountE, modifyStateE)
--}
--}
--{- modes/linters
import qualified Yi.Mode.Haskell                  as     H
--}
--{- locals
import           Arguments                        hiding ((!))
import           Modes
import           Utility
import           Menu3
import           Menus
import           Snippets
--}
--{- new
import           Yi.Search                               (doSearch, SearchOption(IgnoreCase))
import           Yi.Tag                                  (TagTable, setTags)
import           Yi.Types                                (Action(YiA,EditorA))
import qualified Yi.Buffer.HighLevel                  as BH
import qualified Yi.Buffer.Misc                       as B
import qualified Yi.Editor                            as E
import           Yi.File
import           Yi.UI.LineNumbers
import           Yi.TextCompletion
import           Yi.Snippet
--}
--}

main :: IO ()
main = argumentative myConfig

--{- config application
myConfig :: ConfigM ()
myConfig = do
  configureVty

  --{- vim config
  configureVim
  defaultKmA .= myKeymapSet
  modeTableA .= myModes -- <> map (onMode prefIndent) (modeTable V.defVimConfig)
--configCheckExternalChangesObsessivelyA .= False
  --}

  configureHaskellMode
  configureJavaScriptMode
  configureMiscModes
--}

leader :: V.EventString
leader = ","

-- 0=tab
mySpaces :: Int
mySpaces = 0

myKeymapSet :: KeymapSet
myKeymapSet = V.mkKeymapSet $ V.defVimConfig `override` \super this ->
  let eval = V.pureEval this
  in super
    {- Here we can add custom bindings.
       See Yi.Keymap.Vim.Common for datatypes and
       Yi.Keymap.Vim.Utils for useful functions like mkStringBindingE

       In case of conflict, that is if there exist multiple bindings
       whose prereq function returns WholeMatch,
       the first such binding is used.
       So it's important to have custom bindings first.
    -}
    { V.vimBindings = myBindings eval <> V.vimBindings super
    , V.vimExCommandParsers = exParse : V.vimExCommandParsers super
    }


--{- bindings
myBindings :: (V.EventString -> EditorM ()) -> [V.VimBinding]
myBindings eval =
  let
  --{- mapping functions
    --{- multi-mode mapper
    -- takes a list of modes applicable for the binding and action, and maps
    -- them correspondingly | YiM() and EditorM() variants
    -- mapY | mapE
    me = mapper $ V.VimBindingE . mm
    my = mapper $ V.VimBindingY . mm
    -- informed variants of ^ (the action is told the mode...)
    ie = mapper $ V.VimBindingE . \(s,k,a) -> mm (s,k,a s)
    iy = mapper $ V.VimBindingY . \(s,k,a) -> mm (s,k,a s)
--  iy (ts,ks,a) = concat $ map (\k -> map (\t -> V.VimBindingY $ mm t k $ a t) ts ) ks
    -- map the mode(s), and map the key(s)
    mapper f (ss,ks,a) = [f (s,k,a) | s <- ss, k <- ks]
    -- takes a desired mode (string) and handles the rest as VimBindings
    -- :: String -> V.EventString -> monad x -> V.EventString -> V.VimState -> V.MatchResult (monad V.RepeatToken)
    mm (state,key,context) = \key' s ->
      fromMaybe V.NoMatch $ lookup (idMode s)
        [  ("", V.NoMatch
        ), (state, (const $ context >> return V.Finish) <$> key' `V.matchesString` key
        )]
--  fm = mm V.Finish
--  cm = mm V.Continue
--  dm = mm V.Drop
{-- for debugging mode
      case idMode s of
        _ -> (const $ context state (idMode s) (show $ V.vsMode s) >> return V.Continue)
             >> return V.Continue) <$> key' `V.matchesString` key
 -- ...
  , mm  "" "k"    \a b c -> wBuf $ insertN $ fromString $ concat ["~ state:",a,"  id:",b,"  show:",c," ~"]
--}
    --}
  --}
    -- | ins | rep | exr | ser | nor | vis |
    -- | typ ----------------- | ----- nav |
    -- | --> |     | <---------------- noR |
    -- | --------> |     | <---------- noE |
    -- | --------------> |     | <---- noS |
    -- | --------> |           | <---- noM |
    -- | --> |                 | <---- com |
    -- | all ----------------------------- |

  in
    concat $
      map me
      -- TODO use shift instead of ctrl
      [ noM ["<C-n>"        ] ##previousTabE
      , noM ["<C-e>"        ] ##nextTabE
      , noM ["<C-PgUp>"     ] ##previousTabE
      , noM ["<C-PgDn>"     ] ##nextTabE

      , nav ["B"            ] $ wBuf moveToSol
      , nav ["E"            ] $ wBuf moveToEol

--    , nav [" "            ] $ eval ":nohlsearch<CR>"
      , nav [" "            ] $ startMenu (KASCII ' ') mainMenu

--    , nor ["<F3>"         ] $ wBuf deleteTrailingSpaceB
--    , nor ["<F1>"         ] $ wBuf readCurrentWordB >>= printMsg . toText

      , nav ["<Tab>"        ] # wBuf . indent mySpaces
      , com [">>"           ] # wBuf . indent mySpaces
      , com ["<<"           ] # wBuf . indent mySpaces . negate

      , nav [ ",q"          ] $ E.closeBufferAndWindowE
      , nav [",,q"          ] $ E.closeBufferAndWindowE

      --nor ["m"            ] $ do
      --moduleName <- guessModuleName <$> refer filename
      --eval "gg0"
      --insertN $ "module "<>moduleName<>"where\n"

      --{- navigation
      , noM ["<C-Up>"       ] # wBuf . void . lineMoveRel    . negate
      , noM ["<C-Down>"     ] # wBuf . void . lineMoveRel
      , noM ["<Up>"         ] # smartMove . negate
      , noM ["<Down>"       ] # smartMove
      , glo ["<Right>"      ] # wBuf . moveXorEol
      , glo ["<Left>"       ] # wBuf . moveXorSol
      --}

--    , nor ["M"            ] ##test "test"

      , nav ["<F1>"         ] $ wBuf $ setDisplayLineNumbersLocal $ Just True

      , glo ["<Home>"       ] $ wBuf moveToSol
      , glo ["<End>"        ] $ wBuf moveToEol

      -- ex mode
      , nor ["<Esc>",";"    ] $ eval ":"
      ] <>
      map my
      [ nav [ ",w","<C-s>"  ] $ viWrite
      , nav [",,w","<C-S-s>"] $ viWrite
      -- TODO allow S-Tab for raw tab char
      , glo ["<Tab>"        ] $ withEditor $ expander snips
      --nav ["/"            ] $ wordComplete' False
      -- TODO fix
      , nav ["M"            ] $ switchBuffer
      , nav ["<F2>"         ] $ insertCurrentDate
      ] <>
      map ie
      [ com -- ["n-","?-"]
            ["<F4>"         ] $ wBuf . insertN . fromString
      ] <>
      map iy
      [ --  [""             ] \m-> f m
      ]
    where
      ex = do
        void $ spawnMinibufferE ":" id
        switchModeE V.Ex
      mv = wBuf . void
      smartMove n
        | abs n==1  = mv $ lineMoveVisRel n
        | otherwise = mv $ lineMoveRel    n
      wBuf = withCurrentBuffer
      l = leader
--}

--{- count based functions
-- mnemonic - repeat based on number
infixr 1 ##
(##) a b = a $ nestCount  b
-- mnemonic - pass in number
infixr 1 #
(#) a b  = a $ withCount b
withCount f = getCountE >>= f >> resetCountE
nestCount f = do
  count <- getCountE
  replicateM_ (max count 1) f
  resetCountE
--}

myModes :: [AnyMode]
myModes = [
  AnyMode H.fastMode {-
      -- Disable beautification
      modePrettify = const $ return ()
  -}
  ]

--{- commands
helloWorld :: YiM ()
helloWorld = withCurrentBuffer $ insertN "Hello, world!"

exParse :: V.EventString -> Maybe V.ExCommand
exParse = flip lookup $ map (\(n,f) -> (n,cmd n f))
  [ ("hello"     , helloWorld
  ),("sig"       , void genSignatures
--),("reload"    , reload
  )]
  where
    cmd n a = V.impureExCommand { V.cmdAction = YiA a, V.cmdShow = T.pack $ show n }
--}

--{- actions
-- [M]agnitude
indent :: Int -> Int -> BufferM ()
indent spaces m = do
  -- get selection
  r' <- getSelectRegionB
  -- get current location
  Point p  <- pointB
  -- extend the region to full lines
  r  <- unitWiseRegion Line r'
  -- prepend each line with a space
  modifyRegionB (mapLines $ fromString . indent' spaces m . toString) r
  -- return cursor to original position
  moveTo $ Point $ p + (m*counTab spaces)

indent' spaces m
  | m < 1     = dec $ abs m
  | otherwise = inc m
  where
--  drop'While :: (Char -> Bool) -> Int -> YiString
    drop'While _ _ []       = []
    drop'While p n s@(x:xs)
      | n < 1     = s
      | p x       = drop'While p (n-1) xs
      | otherwise = s
    dec n = drop'While (==tabc) $ n * max 1 spaces
    inc n = (concat (replicate n tab) <>)
    tab   = genTabs spaces
    tabc  = head tab

genTabs s = if s > 0 then replicate s ' ' else "\t"
counTab s = xIf (> 0) s 1
--}

--{- helpers
-- Predicate | Value | Default
xIf p v d = if p v then v else d
--}

