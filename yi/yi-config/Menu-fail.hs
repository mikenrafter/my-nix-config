{-# LANGUAGE OverloadedStrings, BlockArguments, LambdaCase, FlexibleContexts #-}
module Menu3 where

--{- standard external
import qualified Data.Text                            as T
import qualified Text.ParserCombinators.Parsec        as P
import           Control.Monad.State.Lazy
import           Lens.Micro.Platform
import           System.Environment
import           Data.Prototype
import           Data.Monoid
import           Data.Maybe
import           Data.List
--}

--{- yi *
import           Yi
import           Yi.Types
import           Yi.Config.Simple.Types
import           Yi.Config
import           Yi.Event
import           Yi.String                               (mapLines)
--port           Yi.Boot                          --     (configMain, reload)
import           Yi.Rope                                 (toText,toString,fromString)
--}
import Yi.Core
import Yi.File
import Yi.MiniBuffer (spawnMinibufferE)

import Control.Monad -- (fmap, void)
import Data.Char     -- (isUpper, toLower)

import Utility

-- | Menu
type Menu      = [MenuElem]
type MenuElem  = (String,MenuItem)
type MenuFn    = MenuContext -> Char -> Keymap
type Generator = EditorM Menu
-- due to frequent usage, lets simplify this...
type E         = EditorM ()

-- | Menu item
data MenuItem
  = MenuAction MenuFn
  | SubMenu    Generator

-- | Menu action context
newtype MenuContext = MenuContext
  { buffer :: BufferRef
--, parent :: Monad Menu
  }


-- | Action on item
action_ :: (YiAction a x, Show x) => String -> a -> MenuElem
action_ title = action title . const

-- | Action on item with context
action :: (YiAction a x, Show x) => String -> (MenuContext -> a) -> MenuElem
action title act =
  (title, MenuAction \ctx ->
  do withEditor closeBufferAndWindowE
     runAction $ makeAction $ act ctx
  )

menu  :: [MenuItem] -> String -> MenuElem
menu  = flip menu' . return
menu' :: String     -> Generator -> MenuElem
menu' = ($$ SubMenu)

startMenu :: Key -> MenuElem -> E
startMenu ekey (_,generator) =
  gets currentBuffer >>= startMenu' . MenuContext where
   startMenu' ctx = display =<< generator where
    display :: [(String,a)] -> E
    display items = void $ spawnMinibufferE menuItems -- $ const
      $ keys mappings actions where
      menuItems = T.pack $ intercalate " | " $ map snd mappings
      mappings  = enkey' $ map fst items
      actions   = map snd items


    exec :: MenuItem -> E
    exec = either ($ctx) (draw =<<) . loadItem where
      loadItem :: MenuItem -> Either MenuFn Generator
      loadItem (MenuAction act) = Left  act
      loadItem (SubMenu  monad) = Right monad


    keys :: [(Char,String)] -> [MenuItem] -> KeymapEndo
    keys cd actions = choice $ map ($cda) [regen,close,toggle] : ca where
      ca  = zipWith mapTo  cd actions :: [Char -> E]
      cda = zipWith cast23 cd actions :: [(Char,String,MenuItem)]
      mapTo (c,d) a = spec (KASCII c) ?>>! exec a
    close    _ = spec KEsc ?>>! kill
    toggle   _ = spec ekey ?>>! kill
    noop       = pure ()
    draw    [] = noop
    draw    is = recurse $ return is
    regen   is = spec (KASCII '-')  ?>>! draw $ map dropFst $ filter ((=='-') . fstOf3) is

    kill       = closeBufferAndWindowE
    recurse is = kill >> startMenu ekey ("",is)


