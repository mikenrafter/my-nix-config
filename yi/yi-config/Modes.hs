{-# LANGUAGE OverloadedStrings, BlockArguments, LambdaCase #-}
module Modes where
--{- imports
--port           Yi.Config.Default.Vim
--port           Yi.Keymap.Vim
import           Yi.Keymap.Vim.Common
--port           Yi.Keymap.Vim.Ex.Types
--port           Yi.Keymap.Vim.Ex.Commands.Common
--port           Yi.Keymap.Vim.Utils
--port           Yi.Keymap.Vim.StateUtils                (switchModeE, resetCountE, modifyStateE)
import           Yi.Types
--port           Yi.Config.Simple.Types
--{- vim
--port           Yi.Config.Default.Vim
--port qualified Yi.Keymap.Vim                    as     V
import qualified Yi.Keymap.Vim.Common             as     V
--port qualified Yi.Keymap.Vim.Ex.Types           as     V
--port qualified Yi.Keymap.Vim.Ex.Commands.Common as     V
--port qualified Yi.Keymap.Vim.Utils              as     V
--port           Yi.Keymap.Vim.StateUtils         --     (switchModeE, resetCountE, modifyStateE)
--}
--}
{- exit bindings, non-functioning atm
exit' m = do
  case
    'i':xs -> {-exitm xs >>-} i
    'r':xs -> {-exitm xs >>-} r
    'e':xs -> {-exitm xs >>-} e
    'n':xs -> {-exitm xs >>-} n
    'v':xs -> {-exitm xs >>-} v
    's':xs -> {-exitm xs >>-} s
    _      -> return Normal
--  _ -> switchModeE Normal
  where
    --{- tweaked default exit bindings
    i = do
      modifyStateE \s -> s
        { V.vsOngoingInsertEvents = mempty
        , V.vsSecondaryCursors    = mempty
        }
      return V.Normal
    r = do
      modifyStateE \s -> s { vsOngoingInsertEvents = mempty }
      withCurrentBuffer $ moveXorSol 1
      return V.Normal
    e = do
      historyFinish
      switchModeE Normal
      closeBufferAndWindowE
      return V.Normal
    n = do
      void $ spawnMinibufferE ":" id
      return V.Ex
    v = do
      clrStatus
      withCurrentBuffer $ do
        setVisibleSelection False
        putRegionStyle Inclusive
      return V.Normal
    s = do
      Search prev _ <- getmode
      isearchCancelE
      return prev
    getmode = fmap V.vsMode getEditorDyn
    --}

--it' :: EditorM () -> EditorM ()
exitm m = do
  count <- getCountE
  when (count > 1) $ do
   inputs <- fmap (parseEvents . vsOngoingInsertEvents) getEditorDyn 
   replicateM_ (count - 1) $ do
     replay defDigraphs inputs
  resetCountE
  mode <- exit' m
  switchModeE mode

--}
--{- mapping presets

ins,  rep,  exr,  ser,  nor,  vis,   typ,  nav,  com,  noR,  noE,  noS,  noM,   glo  :: [V.EventString] -> m0 a0 -> ([String],[V.EventString],m0 a0)
ins', rep', exr', ser', nor', vis',  typ', nav', com', noR', noE', noS', noM',  glo' :: [String]
tre :: [String] -> [V.EventString] -> m0 a0 -> ([String],[V.EventString],m0 a0)

ins  = tre rep'                    -- fn - insert   mode
ins' = ["ii","iI","ia","iA","i-"]
rep  = tre rep' -- fn - replace  mode
rep' = ["r-","rr"]
exr  = tre exr' -- fn - ex       mode
exr' = ["e-"]
ser  = tre ser' -- fn - search   mode
ser' = map ('s':) noS'
nor  = tre nor' -- fn - normal   mode
nor' = ["n-","in"]
vis  = tre vis' -- fn - visual   mode
vis' = ["vi","ve","vl","vb"]

typ  = tre typ' -- fn - typing   modes
typ' = ins'<>rep'<>exr'<>ser'
nav  = tre nav' -- fn - vis+norm modes
nav' = nor'<>vis'
com  = tre com' -- fn - common   modes
com' = nav'<>ins'
noR  = tre noR' -- fn - all-rep  modes
noR' = concat [ins',     exr',ser',nor',vis']
noE  = tre noE' -- fn - all-ex   modes
noE' = concat [ins',rep',     ser',nor',vis']
noS  = tre noS' -- fn - all-ser  modes
noS' = concat [ins',rep',exr',     nor',vis']
noM  = tre noM' -- fn - all-mini modes
noM' = concat [ins',rep',          nor',vis']

glo  = tre glo' -- fn - al la    modes
glo' = noS'<>ser'

tre ss ks a = (ss,ks,a)
--}
--{- known modes
idMode :: VimState -> String
idMode = idMode' . vsMode
idMode' :: VimMode -> String
idMode' = \case
  Insert 'i'             -> "ii"
  Insert 'I'             -> "iI"
  Insert 'a'             -> "ia"
  Insert 'A'             -> "iA"
  Insert  _              -> "i-"
--Insert  x              -> 'i':[x]
  InsertVisual           -> "vi"

  InsertNormal           -> "in"
  Normal                 -> "n-"

  Replace                -> "rr"
  ReplaceSingleChar      -> "r-"

--Visual _               -> "vi"
  Visual Inclusive       -> "vi"
  Visual Exclusive       -> "ve"
  Visual LineWise        -> "vl"
  Visual Block           -> "vb"

  Ex                     -> "e-"

  Search x _             -> 's':(idMode' x)

  _                      -> "?-"
--}
