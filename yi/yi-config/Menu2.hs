module Menu2 where

--{- standard external
import qualified Data.Text                            as T
import qualified Text.ParserCombinators.Parsec        as P
import           Control.Monad.State.Lazy
import           Lens.Micro.Platform
import           System.Environment
import           Data.Prototype
import           Data.Monoid
import           Data.Maybe
import           Data.List
--}

--{- yi *
import           Yi
import           Yi.Types
import           Yi.Config.Simple.Types
import           Yi.Config
import           Yi.Event
import           Yi.String                               (mapLines)
--port           Yi.Boot                          --     (configMain, reload)
import           Yi.Rope                                 (toText,toString,fromString)
--}
import Yi.Core
import Yi.File
import Yi.MiniBuffer (spawnMinibufferE)

import Control.Monad -- (fmap, void)
import Data.Char     -- (isUpper, toLower)

import Utility

-- | Menu
type Menu      = [MenuItem]
type MenuFn    = MenuContext -> Char -> Keymap
type Generator = m0 Menu

-- | Menu item
data MenuItem
  = MenuAction MenuFn
  | SubMenu    Generator

-- | Menu action context
data MenuContext = MenuContext
  { buffer :: BufferRef
  , parent :: m0 Menu
  }


-- | Action on item
action_ :: (YiAction a x, Show x) => String -> a -> MenuItem
action_ title act = action title $ const act

-- | Action on item with context
action :: (YiAction a x, Show x) => String -> (MenuContext -> a) -> MenuItem
action title act = (title, MenuAction \ctx c ->
  char c ?>>! do
    withEditor closeBufferAndWindowE
    runAction $ makeAction $ act ctx
  )

-- | Menu title to keymap
menuEvent :: String -> Maybe (Char, String)
menuEvent t =
  \case
   Nothing -> Nothing
   Just c -> Just (c,t)
  $ toLower <$> find isUpper t


startMenu :: Key -> Menu -> EditorM ()
startMenu ekey menu = do
  items >>= showMenu
  where
    --{- display
    showMenu' :: [(Char,String,a)] -> EditorM ()
    showMenu' csa = void $ spawnMinibufferE menuItems $ const $ subMap csa $ map mapTo csa where
      menuItems = T.pack $ intercalate " | " $ map sndOf3 csa

    showMenu :: [(String, a)] -> EditorM ()
    showMenu is = showMenu' list where
      list    = zipWith (head keyed) mapTo (tail keyed) actions :: [(Char,String,a)]
--    list'   = zip keyed actions :: [((Char,String),a)]
      titles  = map fst is
      actions = map snd is
      keyed   = enkey' titles
--  itms = map \(d,a) -> either only (>>= draw) $ loadItem a
    --}
    --{- handling
    subMap csa da = choice $ map ($csa) [{-regen,-}close,toggle]
      : map snd da
    --}
    --{- keys/generation
    -- TODO add secondary flag
--  draw    is = closeBufferAndWindowE >> showMenu is
--  draw    [] = pure ()
--  regen   is = spec (KASCII '-')  ?>>! draw $ map (\(_,s,a) -> (s,a)) $ filter ((=='-') . fstOf3) is
    close    _ = spec KEsc ?>>! closeBufferAndWindowE
    toggle   _ = spec ekey ?>>! closeBufferAndWindowE
    key title  = fromMaybe (head $ enkey' [title]) $ menuEvent title
    -- Char | Display | Action
    mapTo (c,d,a) = (d,spec (KASCII c) ?>>! a)
    --}


--menu' :: [(String,MenuItem)] -> Key -> Menu
--menu' = 

menu  :: String -> [(String,MenuItem)] -> m0 Menu
menu  = return ()


loadItem :: MenuItem -> Either actn (m0 [MenuItem])
loadItem (MenuAction act) = Left  act
loadItem (SubMenu  monad) = Right monad

only x = x


{-
 -- logic ==
  menu
  [ ("file", menu
   [ ("quit", a askQuitEditor
   ),("save", c fwriteBufferE . buffer
   )]
  ),("edit", menu
   [ ("auto complete", a wordComplete
 --),("completion",    a completeWordB
 --),("sort lines",    a sortLines
   )]
  )]
--}
